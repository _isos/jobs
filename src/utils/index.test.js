import { getSumPointsBy } from "./index";
import { rewards } from "../../input";

describe("Calculate points with criterias", () => {
    it("Should return points by user_id", () => {
        const points = getSumPointsBy(rewards)({ user_id: 4 });
        expect(points).toEqual(80);
    });

    it("Should return points by loyalty_card_id", () => {
        const points = getSumPointsBy(rewards)({ loyalty_card_id: 4 });
        expect(points).toEqual(230);
    });

    it("Should return points by user_id and loyalty_card_id", () => {
        const points = getSumPointsBy(rewards)({ user_id: 6, loyalty_card_id: 1 });
        expect(points).toEqual(18);
    });

    it("Should return 0 with wrong Ids", () => {
        const points = getSumPointsBy(rewards)({ user_id: 17, loyalty_card_id: 1 });
        const points2 = getSumPointsBy(rewards)({ user_id: -3 });
        const points3 = getSumPointsBy(rewards)({ loyalty_card_id: 11 });

        expect(points).toEqual(0);
        expect(points2).toEqual(0);
        expect(points3).toEqual(0);
    });
});
