import { filter, map, reduce, whereEq } from "ramda";

export const getSumPointsBy = rewards => criteria => {
    const hasSameCriteria = whereEq(criteria);
    const filteredRewards = filter(hasSameCriteria, rewards);
    const rewardsPoints = map(({ points }) => points, filteredRewards);

    return reduce((a, b) => a + b, 0, rewardsPoints);
};
