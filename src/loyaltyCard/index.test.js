import { mapUserLoyaltyCards, mapLoyaltyCard } from "./index";
import { loyalty_cards, rewards } from "../../input";
import { getSumPointsBy } from "../utils";

describe("Map loyalty card object from input", () => {

    it("Should return loyalty card info", () => {
        const loyaltyCard = mapLoyaltyCard(loyalty_cards)(3);

        expect(loyaltyCard).toEqual({
            id: 3,
            name: "MacDo"
        })
    });

    it("Should return loyalty card info with points", () => {
        const loyaltyCard = mapLoyaltyCard(loyalty_cards)(3, 40);

        expect(loyaltyCard).toEqual({
            id: 3,
            name: "MacDo",
            total_points: 40
        })
    });

    it("Should return an empty object", () => {
        const loyaltyCard = mapLoyaltyCard(loyalty_cards)(13);
        const loyaltyCard2 = mapLoyaltyCard(loyalty_cards)(13, 40);
        const loyaltyCard3 = mapLoyaltyCard()(13);
        const loyaltyCard4 = mapLoyaltyCard()(13, 40);

        expect(loyaltyCard).toEqual({});
        expect(loyaltyCard2).toEqual({});
        expect(loyaltyCard3).toEqual({});
        expect(loyaltyCard4).toEqual({});
    })

});

describe("Map User loyalty card(s) from input", () => {
    const getPointsBy = getSumPointsBy(rewards);
    const getLoyaltyCard = mapLoyaltyCard(loyalty_cards);

    it('Should return a user\'s loyalty cards', () => {
        const loyaltyCards = mapUserLoyaltyCards(getPointsBy, getLoyaltyCard, rewards)(1);

        expect(loyaltyCards).toEqual([
            {"id": 1, "points": 26, "name": "Carrefour"},
            {"id": 4, "points": 131, "name": "Fnac"}
        ]);
    });

    it('Should return an empty array when there is a wrong userID', () => {
        const loyaltyCards = mapUserLoyaltyCards(getPointsBy, getLoyaltyCard, rewards)(9);

        expect(loyaltyCards).toEqual([]);
    });

    it('Should return an empty array without the required functions and input', () => {
        const loyaltyCards = mapUserLoyaltyCards()(1);

        expect(loyaltyCards).toEqual([]);
    });

    it('Should return an empty array without the required functions', () => {
        const loyaltyCards = mapUserLoyaltyCards(null, null, rewards)(1);

        expect(loyaltyCards).toEqual([]);
    })
});
