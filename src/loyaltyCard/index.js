import { filter, find, map, propEq, uniq, isNil } from "ramda";

export const mapLoyaltyCard = (loyaltyCards = []) => (cardId, points = 0) => {
    const loyaltyCard = find(propEq('id', cardId), loyaltyCards);

    return {
        ...loyaltyCard,
        ...(loyaltyCard && points > 0 && { total_points: points })
    };
};

export const mapUserLoyaltyCards = (getPointsBy, getLoyaltyCard, rewards = []) => userId => {
    if (isNil(getPointsBy) || isNil(getLoyaltyCard)) return  [];
    const filteredRewards = filter(propEq('user_id', userId), rewards);

    const loyaltyCards = map(({ loyalty_card_id }) => {
        const { id, name } = getLoyaltyCard(loyalty_card_id);
        return {
            id,
            points: getPointsBy({ user_id: userId, loyalty_card_id }),
            name
        };
    }, filteredRewards);

    return uniq(loyaltyCards);
};
