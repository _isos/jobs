import { isNil, match, isEmpty } from 'ramda';

import { rewards, loyalty_cards } from "../input"
import { getSumPointsBy } from "./utils";
import { mapLoyaltyCard, mapUserLoyaltyCards } from "./loyaltyCard";

const getPointsBy = getSumPointsBy(rewards);
const getLoyaltyCard = mapLoyaltyCard(loyalty_cards);
const getUserLoyaltyCards = mapUserLoyaltyCards(getPointsBy, getLoyaltyCard, rewards);

const getUser = userId => ({
    id: userId,
    total_points: getPointsBy({user_id: userId}),
    loyalty_cards: getUserLoyaltyCards(userId)
});

const retrieveIdsFromCli = args => {
    const userIdArg = match(/--user_id=([0-9]*)/, args).pop();
    const cardIdArg = match(/--loyalty_card_id=([0-9]*)/, args).pop();
    const userId = isNil(userIdArg) || isEmpty(userIdArg) ? null : parseInt(userIdArg);
    const cardId = isNil(cardIdArg) || isEmpty(cardIdArg) ? null : parseInt(cardIdArg);

    return { userId, cardId };
};

const main = (args = []) => {
    const { userId, cardId } = retrieveIdsFromCli(args.toString());
    const loyaltyCardPoints = getPointsBy({ loyalty_card_id: cardId });
    const loyalty_card = getLoyaltyCard(cardId, loyaltyCardPoints);
    const user = getUser(userId);

    const result = {
        ...(!isEmpty(user.loyalty_cards) && { user }),
        ...(!isEmpty(loyalty_card) && { loyalty_card })
    };

    console.log(JSON.stringify(result, null, 2));
    return result;
};

main(process.argv.slice(2));

export default main;
