import main from "./index";

describe("FidMe basic use", () => {
   it('should return a user and a loyalty card', () => {
      const { user, loyalty_card } = main(['--user_id=5', '--loyalty_card_id=1']);

      expect(user.id).toEqual(5);
      expect(loyalty_card.id).toEqual(1);
   });

   it('should return a user', () => {
      const { user, loyalty_card } = main(['--user_id=5']);

      expect(user.id).toEqual(5);
      expect(loyalty_card).toBeUndefined();
   });

   it('should return a loyalty_card', () => {
      const { user, loyalty_card } = main(['--loyalty_card_id=3']);

      expect(user).toBeUndefined();
      expect(loyalty_card.id).toEqual(3);
   });

   it('should be empty', () => {
      const data = main();
      expect(data).toEqual({});
   });

   it('should be empty (2)', () => {
      const data = main(['--user_id=16', '--loyalty_card_id=-3']);
      expect(data).toEqual({});
   });
});